@extends('layouts.layout',['title'=>'membres'])
@section('content')
       <main>
        <!--? Hero Start -->
        <div class="slider-area ">
            <div class="slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>MEMBRES</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!--? Team Ara Start -->
        <div class="team-area pt-160 pb-160">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="utilisateurs/assets/img/membres/DG.jpg" alt="">
                                <!-- Blog Social -->
                                {{-- <ul class="team-social">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fas fa-globe"></i></a></li>
                                </ul> --}}
                            </div>
                            <div class="team-caption">
                                <h3><a href="">EDJAM Kossi</a></h3>
                                <p>Directeur</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="utilisateurs/assets/img/membres/2.jpg" alt="">
                                <!-- Blog Social -->
                                {{-- <ul class="team-social">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fas fa-globe"></i></a></li>
                                </ul> --}}
                            </div>
                            <div class="team-caption">
                                <h3><a href="">TERAOU Amoumagnim</a></h3>
                                <p>Division de l'information et de la documentation</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="utilisateurs/assets/img/membres/3.jpg" alt="">
                                <!-- Blog Social -->
                                {{-- <ul class="team-social">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fas fa-globe"></i></a></li>
                                </ul> --}}
                            </div>
                            <div class="team-caption">
                                <h3><a href="#">GBADAMASSI Souléjka</a></h3>
                                <p>Division de la production</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="utilisateurs/assets/img/membres/4.jpg" alt="">
                                <!-- Blog Social -->
                                {{-- <ul class="team-social">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fas fa-globe"></i></a></li>
                                </ul> --}}
                            </div>
                            <div class="team-caption">
                                <h3><a href="">HAINGA Tchaa</a></h3>
                                <p>Chef section information et orientation scolaire et professionnelle</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="utilisateurs/assets/img/membres/5.jpg" alt="">
                                <!-- Blog Social -->
                                {{-- <ul class="team-social">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fas fa-globe"></i></a></li>
                                </ul> --}}
                            </div>
                            <div class="team-caption">
                                <h3><a href="">NAWO Assoura</a></h3>
                                <p> Section diffusion et pubication</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="utilisateurs/assets/img/membres/6.jpg" alt="">
                                <!-- Blog Social -->
                                {{-- <ul class="team-social">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fas fa-globe"></i></a></li>
                                </ul> --}}
                            </div>
                            <div class="team-caption">
                                <h3><a href="">AMEGAH Tchinde Tokore Hassoh</a></h3>
                                <p>Section enquête </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="utilisateurs/assets/img/membres/7.jpg" alt="">
                                <!-- Blog Social -->
                                {{-- <ul class="team-social">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fas fa-globe"></i></a></li>
                                </ul> --}}
                            </div>
                            <div class="team-caption">
                                <h3><a href="">LAMBONI Damigou</a></h3>
                                <p>Section documentation</p>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="utilisateurs/assets/img/membres/9.jpg" alt="">
                                <!-- Blog Social -->
                                {{-- <ul class="team-social">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fas fa-globe"></i></a></li>
                                </ul> --}}
                            </div>
                            <div class="team-caption">
                                <h3><a href="">AMEGAN Silvie</a></h3>
                                <p>Comptable</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="utilisateurs/assets/img/membres/8.jpg" alt="">
                                <!-- Blog Social -->
                                {{-- <ul class="team-social">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fas fa-globe"></i></a></li>
                                </ul> --}}
                            </div>
                            <div class="team-caption">
                                <h3><a href="">LENLI Ramatou</a></h3>
                                <p>Sécretaire</p>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="utilisateurs/assets/img/membres/DG.jpg" alt="">
                               
                            </div>
                            <div class="team-caption">
                                <h3><a href="">ALOYE  Pèhèzibadi</a></h3>
                                <p>Opérateur de saisie</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="utilisateurs/assets/img/membres/DG.jpg" alt="">
                               
                            </div>
                            <div class="team-caption">
                                <h3><a href="">TCHEKPI M'bèlou</a></h3>
                                <p>Coursier</p>
                            </div>
                        </div>
                    </div>
                    --}}
                
                </div>
            </div>
        </div>
        <!-- Team Ara End -->
    </main>
@endsection

 
 