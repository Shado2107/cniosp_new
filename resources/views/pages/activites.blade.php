@extends('layouts.layout',['title'=>'activites'])

@section('content')

<main>
    <!--? Hero Start -->
    <div class="slider-area">
        <div class="slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap hero-cap2 text-center">
                            <h2>NOS ACTIVITES</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero End -->

    <!-- About Details Start -->
    <div class="about-details section-padding30">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="about-details-cap mb-50">
                        <h4>Activités du CNIOSP</h4>
                        <p>Pour pouvoir atteindres ses objectifs, le CNIOSP mène des activités suivantes :
                            
                            <ul class="unordered-list">
                            
                                <li>Assure la prise en charge des élèves en difficultés scolaires dans les écoles primaires ;
                                    {{-- <ul>
                                        <li>la division de la Documentation,
                                            <ul>
                                                <li>Protective Preventative Maintenance</li>
                                            </ul>
                                        </li>
                                        <li>la division de l’Enquête et Statistique,</li>
                                        <li>la division de la Diffusion et de la Publication,</li>
                                        <li>la division de l’Orientation Scolaire et Professionnelle.</li>
                                    </ul> --}}
                                </li>
                                <li>Enseignement ou éducation à l’orientation par la passation des modules sur l’orientation dans les classes, </li>
                                <li>Entretiens individuels d’orientation, d’aide et d’appui-conseil aux apprenants </li>
                                <li>Organisation des journées d’information et d’information dans les établissements scolaires du 
                                secondaire ;</li>
                                <li>Passation des tests psychotechniques aux bénéficiaires ; </li>
                                <li>Animation des émissions radio télévisées sur les médias ; </li>
                            </ul>
                        </p>
                        <p>
                            Toutes les thématiques abordées sont relatifs à :
                            
                            <ul class="unordered-list">
                            
                                <li>Connaissance le système éducatif dans son ensemble et plus particulièrement l’environnement du collège ;</li>
                                <li>Respect l’autorité de l’institution scolaire et prendre conscience de son importance dans leur devenir, </li>
                                <li>Adhésion harmonieusement au processus de scolarisation au collège ; </li>
                                <li>Comment réussir ses apprentissages de façon efficace ;</li>
                                <li>Persévérance scolaire et le maintien dans les différents parcours ;</li>
                                <li>Réussite scolaire et son rapport avec le processus d’orientation  </li>
                                <li>Elaboration et la mise en œuvre un projet scolaire et professionnel ;  </li>
                                <li>Connaissance des séries et les filières de formation professionnelles après le BEPC ;  </li>
                                <li>Critères à prendre en compte pour une bonne orientation ;  </li>
                                <li>Offres de formation après le BAC 2 (facultés, instituts et écoles des universités publiques et des écoles de formation professionnelle du Togo) et leurs conditions d’accès ;  </li>
                                <li>Enjeux du système LMD dans les universités publiques du Togo ;  </li>
                                <li>Critères d’éligibilité et conditions d’attributions des bourses et allocations de secours ;  </li>
                                <li>Informations sur les filières porteuses d’insertion professionnelle et les formations professionnelles pour y accéder ;  </li>
                            </ul>                
                        </p>
                        <p>
                            <h3>Voici en images certaines de nos activités</h3>
                       </p>
                    </div>
                    <div class="row gallery-item">
                        <div class="col-md-4">
                            <a href="utilisateurs/assets/img/elements/g1.jpg" class="img-pop-up">
                                <div class="single-gallery-image" style="background: url(utilisateurs/assets/img/elements/g1.jpg);"></div>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="utilisateurs/assets/img/elements/g2.jpg" class="img-pop-up">
                                <div class="single-gallery-image" style="background: url(utilisateurs/assets/img/elements/g2.jpg);"></div>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="utilisateurs/assets/img/elements/g3.jpg" class="img-pop-up">
                                <div class="single-gallery-image" style="background: url(utilisateurs/assets/img/elements/g3.jpg);"></div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="utilisateurs/assets/img/elements/g4.jpg" class="img-pop-up">
                                <div class="single-gallery-image" style="background: url(utilisateurs/assets/img/elements/g4.jpg);"></div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="utilisateurs/assets/img/elements/g5.jpg" class="img-pop-up">
                                <div class="single-gallery-image" style="background: url(utilisateurs/assets/img/elements/g5.jpg);"></div>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="utilisateurs/assets/img/elements/g6.jpg" class="img-pop-up">
                                <div class="single-gallery-image" style="background: url(utilisateurs/assets/img/elements/g6.jpg);"></div>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="assets/img/elements/g7.jpg" class="img-pop-up">
                                <div class="single-gallery-image" style="background: url(utilisateurs/assets/img/elements/g7.jpg);"></div>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="assets/img/elements/g8.jpg" class="img-pop-up">
                                <div class="single-gallery-image" style="background: url(utilisateurs/assets/img/elements/g8.jpg);"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Details End -->
    <!--? Count Down Start -->
    <!-- Testimonial End -->
    <!--? About  Start-->
    <div class="about-area section-padding2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="about-caption mb-50">
                        <!-- Section Tittle -->
                        <div class="section-tittle mb-35">
                            <span>More About Our Company</span>
                            <h2>Want to know more</h2>
                        </div>
                        <p>There arge many variations ohf passages of sorem gpsum ilable, but the majority have suffered alteration in some form, by ected humour, or randomised words whi.</p>
                        <ul>
                            <li><span class="flaticon-business"></span> Creative ideas base</li>
                            <li><span class="flaticon-communications-1"></span> Assages of sorem gpsum ilable</li>
                            <li><span class="flaticon-graduated"></span> Have suffered alteration in so</li>
                            <li><span class="flaticon-tools-and-utensils"></span> Randomised words whi</li>
                        </ul>
                        <a href="about.html" class="btn">More About Us</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <!-- about-img -->
                    <div class="about-img ">
                        <div class="about-font-img d-none d-lg-block">
                            <img src="utilisateurs/assets/img/gallery/about2.png" alt="">
                        </div>
                        <div class="about-back-img ">
                            <img src="utilisateurs/assets/img/gallery/about1.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Law End-->
</main>

    
@endsection