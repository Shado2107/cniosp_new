@extends('layouts.layout', ['title'=>'actualités'])
@section('content')
       
    <main>
        <!--? Hero Start -->
        <div class="slider-area ">
            <div class="slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>ACTUALITES</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
   
        <main>
          
            <div class="categories-area section-padding30">
                <div class="container">
                    <div class="row justify-content-sm-center">
                        <div class="cl-xl-4 col-lg-8 col-md-10">
                            <!-- Section Tittle -->
                            <div class="section-tittle text-center mb-70">
                                <span>Restez informés sur toutes nos activités</span>
                                {{-- <h2>Actualités</h2> --}}
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($actualites as $actu)
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-cat mb-50">
                                <div class="cat-icon">
                                    <span class="flaticon-edit"></span>
                                </div>
                                <div class="cat-cap">
                                    <h5><a  href="{{ route('detailsactu', $actu->id)}}"> {{$actu->titre}} </a></h5>
                                    <p> {{$actu->resume}} </p>
                                    <a href="{{ route('detailsactu', $actu->id)}}" class="read-more1">Lire plus></a>
                                </div>
                            </div>
                        </div>

                        {{$actualites->links()}}

                        @endforeach
                        
                        {{-- <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-cat mb-50">
                                <div class="cat-icon">
                                    <span class="flaticon-education"></span>
                                </div>
                                <div class="cat-cap">
                                    <h5><a  href="#">App Development</a></h5>
                                    <p>Sorem hpsum folor sixdsft amhtget, consectetur adipiscing eliht, sed do eiusmod tempor incidi.</p>
                                    <a  href="#" class="read-more1">Read More ></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-cat mb-50">
                                <div class="cat-icon">
                                    <span class="flaticon-communications"></span>
                                </div>
                                <div class="cat-cap">
                                    <h5><a  href="#">Video Editing</a></h5>
                                    <p>Sorem hpsum folor sixdsft amhtget, consectetur adipiscing eliht, sed do eiusmod tempor incidi.</p>
                                    <a  href="#" class="read-more1">Read More ></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-cat mb-50">
                                <div class="cat-icon">
                                    <span class="flaticon-computing"></span>
                                </div>
                                <div class="cat-cap">
                                    <h5><a  href="#">Digital Marketing</a></h5>
                                    <p>Sorem hpsum folor sixdsft amhtget, consectetur adipiscing eliht, sed do eiusmod tempor incidi.</p>
                                    <a  href="#" class="read-more1">Read More ></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-cat mb-50">
                                <div class="cat-icon">
                                    <span class="flaticon-tools-and-utensils"></span>
                                </div>
                                <div class="cat-cap">
                                    <h5><a  href="#">Seo Marketing</a></h5>
                                    <p>Sorem hpsum folor sixdsft amhtget, consectetur adipiscing eliht, sed do eiusmod tempor incidi.</p>
                                    <a  href="#" class="read-more1">Read More ></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-cat mb-50">
                                <div class="cat-icon">
                                    <span class="flaticon-business"></span>
                                </div>
                                <div class="cat-cap">
                                    <h5><a  href="#">Content Writing</a></h5>
                                    <p>Sorem hpsum folor sixdsft amhtget, consectetur adipiscing eliht, sed do eiusmod tempor incidi.</p>
                                    <a  href="#" class="read-more1">Read More ></a>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <!-- Section Button -->
                    <div class="row">
                        <div class="col-lg-12">
                            {{-- <div class="browse-btn2 text-center mt-50">
                                <a href=" {{route('actualites')}} " class="btn">Plus d'actualités</a>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
                 
   
   
@endsection

 
 