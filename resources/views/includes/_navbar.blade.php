<header>
    <!-- Header Start -->
    <div class="header-area">
        <div class="main-header ">
            <div class="header-top d-none d-lg-block">
                <!-- Left Social -->
                <div class="header-left-social">
                    <ul class="header-social">    
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/sai4ull"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        {{-- <li> <a href="#"><i class="fab fa-google-plus-g"></i></a></li> --}}
                    </ul>
                </div>
                <div class="container">
                    <div class="col-xl-12">
                        <div class="row d-flex justify-content-between align-items-center">
                            <div class="header-info-left">
                                <ul>     
                                    <li>info@cniosp.tg </li>
                                    <li>22 21 30 47 / 22 21 60 43 </li>
                                </ul>
                            </div>
                            <div class="header-info-right">
                                <ul>    
                                    {{-- <li><a href="#"><i class="ti-user"></i>Se connecter</a></li> --}}
                                    {{-- <li><a href="#"><i class="ti-lock"></i>Register</a></li> --}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom header-sticky">
                <!-- Logo -->
                <div class="logo d-none d-lg-block">
                    <a href=""><img src="utilisateurs/assets/img/logo/logo.png" alt=""></a>
                </div>
                <div class="container">
                    <div class="menu-wrapper">
                        <!-- Logo -->
                        <div class="logo logo2 d-block d-lg-none">
                            <a href=""><img src="utilisateurs/assets/img/logo/logo.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
                        <div class="main-menu d-none d-lg-block">
                            <nav>
                                <ul id="navigation">                                                                                          
                                    <li><a href="/">CNIOSP</a></li>
                                    <li><a href="{{ route('historique') }}">HISTORIQUE</a></li>
                                    <li><a href="{{ route('apropos') }}">A PROPOS</a>
                                        <ul class="submenu">
                                            <li><a href="{{ route('apropos') }}">a propos</a></li>
                                           
                                            <li><a href=" {{ route('membre') }} ">membres</a></li>
                                            
                                        </ul>
                                    </li>
                                    <li><a href="{{ route('blog') }}">BLOG</a></li>
                                    <li><a href="{{ route('actualites') }}">ACTUALITES</a></li>
            
                                    {{-- <li><a href="blog.html">Blog</a>
                                        <ul class="submenu">
                                            <li><a href="blog.html">Blog</a></li>
                                            <li><a href="blog_details.html">Blog Details</a></li>
                                            <li><a href="elements.html">Element</a></li>
                                        </ul>
                                    </li> --}}
                                    <li><a href=" {{ route('contact') }} ">CONTACT</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Header-btn -->
                        {{-- <div class="header-search d-none d-lg-block">
                            <form action="#" class="form-box f-right ">
                                <input type="text" name="Search" placeholder="Search Courses">
                                <div class="search-icon">
                                    <i class="fas fa-search special-tag"></i>
                                </div>
                            </form>
                        </div> --}}
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->
</header>

