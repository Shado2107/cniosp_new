<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
     <title> {{setting('site.title')}} | {{isset($title) ?$title.'':''}}</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href=" {{asset('utilisateurs/assets/img/logo/loder.png')}} ">

	<!-- CSS here -->
	<link rel="stylesheet" href="{{asset('utilisateurs/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('utilisateurs/assets/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('utilisateurs/assets/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset('utilisateurs/assets/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('utilisateurs/assets/css/gijgo.css')}}">
	<link rel="stylesheet" href="{{asset('utilisateurs/assets/css/animate.min.css')}}">
	<link rel="stylesheet" href="{{asset('utilisateurs/assets/css/magnific-popup.css')}}">
	<link rel="stylesheet" href="{{asset('utilisateurs/assets/css/fontawesome-all.min.css')}}">
	<link rel="stylesheet" href="{{asset('utilisateurs/assets/css/themify-icons.css')}}">
	<link rel="stylesheet" href="{{asset('utilisateurs/assets/css/slick.css')}}">
	<link rel="stylesheet" href="{{asset('utilisateurs/assets/css/nice-select.css')}}">
	<link rel="stylesheet" href="{{asset('utilisateurs/assets/css/style.css')}}">
</head>

    

    