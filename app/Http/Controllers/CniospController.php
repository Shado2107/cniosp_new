<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Membre;
use App\Actualite;
use App\Article;
use DB;

class CniospController extends Controller
{
    public function About(){
        return view('pages.about');
    }
    
    public function Historique(){
        return view('pages.historique');
    }
    
    public function Activites(){
        return view('pages.activites');
    }
    
    public function Membres(){
        $membres= Membre::all();
        return view('pages.membre',[
            'membres'=>$membres,
        ]);
    }
       
    public function Contact(){
        return view('pages.contact');
    }

    public function Blog(){
        $autre= Article::all()->take(10);
        // $article= Article::all()->paginate(3);
        $article=DB::table('articles')->paginate(3);
        return view('pages.blog',[
            'articles'=> $article,
            'autre' => $autre,
        ]);
    }

    public function Actualites(){
        // $actu= Actualite::all()->paginate(12);
        $actu=DB::table('actualites')->paginate(12);
        return view('pages.actualites',[
            'actualites'=> $actu,
        ]);
    }


    public function DetailsActualite($id){
        $autre= Actualite::all()->take(10);
        $article = Actualite::where('id',$id)->first();
        $prev = Actualite::where('id', '<', $id)->where('statut','=','publie')->orderBy('id','desc')->first();
        $next = Actualite::where('id', '>', $id)->where('statut','=','publie')->orderBy('id')->first();
        
        return view('pages.detailsactu',[
            'article' => $article,
            'precedent' => $prev,
            'suivant' => $next,
            'autre' => $autre,
        ]);
    }


    public function DetailsBlog($id){
        $autre= Article::all()->take(10);
        $article = Article::where('id',$id)->first();
        $prev = Article::where('id', '<', $id)->where('statut','=','publie')->orderBy('id','desc')->first();
        $next = Article::where('id', '>', $id)->where('statut','=','publie')->orderBy('id')->first();
        
        return view('pages.detailsblog',[
            'article' => $article,
            'precedent' => $prev,
            'suivant' => $next,
            'autre' => $autre,
        ]);
    }


}
